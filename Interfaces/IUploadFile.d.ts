import { Entry } from '../Objects/Entry';
import { BaseOptions } from './Base';
export declare type UploadFileCallBack = (uploadResponse: UploadResponse) => void;
export interface UploadFileOptions extends BaseOptions {
    fileId: number;
    boxFolderId: number;
    Created: UploadFileCallBack;
}
export interface UploadResponse {
    total_count: number;
    entries: Entry[];
}
