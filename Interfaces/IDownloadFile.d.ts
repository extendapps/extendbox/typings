import { BaseOptions } from './Base';
export declare type DownloadFileCallBack = (details: DownloadFileDetails) => void;
export interface DownloadFileOptions extends BaseOptions {
    file_id: string;
    version?: string;
    OK: DownloadFileCallBack;
}
export interface DownloadFileDetails {
    contents: string;
    contentType: string;
}
