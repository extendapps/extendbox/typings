import { WebHook } from '../Objects/WebHook';
import { Target } from '../Objects/Target';
import { BaseOptions } from './Base';
export declare type WebHookCallBack = (webHook: WebHook) => void;
export declare type WebHooksCallBack = (webhooks: WebHooksPayload) => void;
export interface BaseWebHookOptions extends BaseOptions {
    target: Target;
    triggers: string[];
}
export interface GetWebHooksOptions extends BaseOptions {
    OK: WebHooksCallBack;
}
export interface GetWebHookOptions extends BaseOptions {
    webhook_id: string;
    OK: WebHookCallBack;
}
export interface RegisterWebHookOptions extends BaseWebHookOptions {
    Created: WebHookCallBack;
}
export interface UpdateWebHookOptions extends BaseWebHookOptions {
    webhook_id: string;
    OK: WebHookCallBack;
}
export interface WebHooksPayload {
    next_marker: string;
    entries: WebHook[];
    limit: number;
}
export interface DeleteWebHookOptions extends BaseOptions {
    webhook_id: string;
    Deleted: () => void;
}
