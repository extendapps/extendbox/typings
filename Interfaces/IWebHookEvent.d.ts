import { WebHookEvent } from '../Objects/WebHookEvent';
import { BOX_API } from '../BOX_API';
export declare type WebHookEventFunction = (webhookEvent: WebHookEvent, box: BOX_API) => void;
export interface WebHookEventHandler {
    processWebHookEvent: WebHookEventFunction;
}
