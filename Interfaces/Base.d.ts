import * as https from 'N/https';
export interface BaseOptions {
    fail?: FailedCallback;
}
export declare type FailedCallback = (clientResponse: https.ClientResponse) => void;
