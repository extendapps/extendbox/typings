import { AccessToken } from '../Objects/AccessToken';
import { BaseOptions } from './Base';
export declare type AccessTokenCallBack = (accessTokenResponse: AccessToken) => void;
export interface BaseAccessTokenOptions extends BaseOptions {
    client_id: string;
    client_secret: string;
    OK: AccessTokenCallBack;
}
export interface RefreshAccessTokenOptions extends BaseAccessTokenOptions {
    refreshToken: string;
}
export interface GetAccessTokenOptions extends BaseAccessTokenOptions {
    code: string;
}
