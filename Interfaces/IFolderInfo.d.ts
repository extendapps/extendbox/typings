import { Entry } from '../Objects/Entry';
import { BaseOptions } from './Base';
export interface GetFolderInfoOptions extends BaseOptions {
    folder_id: string;
    OK: (folderInfo: Entry) => void;
}
