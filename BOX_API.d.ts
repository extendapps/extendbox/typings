import { UploadFileOptions } from './Interfaces/IUploadFile';
import { DownloadFileOptions } from './Interfaces/IDownloadFile';
import { GetFolderInfoOptions } from './Interfaces/IFolderInfo';
export interface BOX_API {
    uploadFile(options: UploadFileOptions): void;
    downloadFile(options: DownloadFileOptions): void;
    getFolderInfo(options: GetFolderInfoOptions): void;
}
