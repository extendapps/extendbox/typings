export interface Target {
    id: string;
    type: 'folder' | 'file';
}
