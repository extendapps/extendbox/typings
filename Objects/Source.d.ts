import { Entry } from './Entry';
import { FileVersion } from './FileVersion';
import { PathCollection } from './PathCollection';
import { User } from './User';
export interface Source {
    id: string;
    type: string;
    file_version: FileVersion;
    sequence_id: string;
    etag: string;
    sha1: string;
    name: string;
    description: string;
    size: number;
    path_collection: PathCollection;
    created_at: Date;
    modified_at: Date;
    trashed_at?: any;
    purged_at?: any;
    content_created_at: Date;
    content_modified_at: Date;
    created_by: User;
    modified_by: User;
    owned_by: User;
    shared_link?: any;
    parent: Entry;
    item_status: string;
}
