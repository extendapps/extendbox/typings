import { Entry } from './Entry';
export interface PathCollection {
    total_count: number;
    entries: Entry[];
}
