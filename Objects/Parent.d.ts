export interface Parent {
    type: string;
    id: string;
    sequence_id: string;
    etag: string;
    name: string;
}
