export interface AccessToken {
    access_token: string;
    expires_in: number;
    restricted_to: any[];
    refresh_token: string;
    token_type: string;
}
