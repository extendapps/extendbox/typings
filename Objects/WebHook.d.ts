import { Target } from './Target';
import { User } from './User';
export interface WebHook {
    id: string;
    type: string;
    target: Target;
    created_by: User;
    created_at: Date;
    address: string;
    triggers: string[];
}
