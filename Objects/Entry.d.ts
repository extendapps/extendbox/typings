import { User } from './User';
import { Parent } from './Parent';
export interface Entry {
    type: string;
    id: string;
    sequence_id: string;
    etag: string;
    sha1: string;
    name: string;
    description: string;
    size: number;
    path_collection: Entry;
    created_at: Date;
    modified_at: Date;
    trashed_at?: any;
    purged_at?: any;
    content_created_at: Date;
    content_modified_at: Date;
    created_by: User;
    modified_by: User;
    owned_by: User;
    shared_link?: any;
    parent: Parent;
    item_status: string;
}
