export interface FileVersion {
    type: string;
    id: string;
    sha1: string;
}
