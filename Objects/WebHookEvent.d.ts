import { Source } from './Source';
import { WebHook } from './WebHook';
import { User } from './User';
export interface WebHookEvent {
    type: string;
    id: string;
    created_at: Date;
    trigger: string;
    webhook: WebHook;
    created_by: User;
    source: Source;
    additional_info: any[];
}
