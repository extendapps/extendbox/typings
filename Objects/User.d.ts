export interface User {
    type: string;
    id: string;
    name: string;
    login: string;
    created_at: Date;
    modified_at: Date;
    language: string;
    space_amount: number;
    space_used: number;
    max_upload_size: number;
    status: string;
    job_title: string;
    phone: string;
    address: string;
    avatar_url: string;
}
